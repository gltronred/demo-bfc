{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}

module Server where

import Api
import Types

import Control.Monad.IO.Class
import Data.Maybe
import Data.Proxy
import Data.Word
import Servant

theProgram :: Program
theProgram = [MoveLeft,MoveRight,LBr,Minus,RBr,Print]

theMem :: Mem
theMem = Mem $ replicate 256 0

programGet :: Handler Program
programGet = do
  liftIO $ putStrLn "GET /program"
  pure theProgram

programPost :: Program -> Handler Program
programPost prog = do
  liftIO $ putStrLn $ "POST /program -- " ++ show prog
  programGet

memGet :: Handler Mem
memGet = do
  liftIO $ putStrLn "GET /memory"
  pure theMem

memPost :: Word -> Word8 -> Handler Mem
memPost addr val = do
  liftIO $ putStrLn $ "PUT /memory -- addr=" ++ show addr ++ "; val=" ++ show val
  pure theMem

inputGet :: Maybe String -> Handler [Input]
inputGet starts = do
  liftIO $ putStrLn $ "GET /inputs -- starts=" ++ fromMaybe "<NA>" starts
  pure []

inputPost :: Input -> Handler Input
inputPost input = do
  liftIO $ putStrLn $ "POST /inputs -- " ++ input
  pure []

outputGet :: Handler [Output]
outputGet = do
  liftIO $ putStrLn "GET /outputs"
  pure []

server :: Server BfcApi
server = programGet
  :<|> programPost
  :<|> memGet
  :<|> memPost
  :<|> inputGet
  :<|> inputPost
  :<|> outputGet

app1 :: Application
app1 = serve (Proxy :: Proxy BfcApi) server

