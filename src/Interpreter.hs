module Interpreter where

import Types

interpret :: Program -> Mem -> Input -> (Mem, Output)
interpret program mem input = (mem, output)
  where output = "Implement me!"

