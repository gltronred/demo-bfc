{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Types where

import Data.Aeson
import Data.Word
import GHC.Generics

data Cmd
  = Plus
  | Minus
  | MoveLeft
  | MoveRight
  | Input
  | Print
  | LBr
  | RBr
  deriving (Eq,Show,Read,Generic)
instance FromJSON Cmd
instance ToJSON Cmd

type Program = [Cmd]

newtype Mem = Mem { getMem :: [Word8] }
  deriving (Eq,Show,Read,FromJSON,ToJSON)

type Input = [Char]

type Output = [Char]

