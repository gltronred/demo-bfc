{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Api where

import Types

import Data.Word
import Servant.API

type BfcApi
  =    "program" :> Get '[JSON] Program
  :<|> "program" :> ReqBody '[JSON] Program :> Post '[JSON] Program
  :<|> "memory" :> Get '[JSON] Mem
  :<|> "memory" :> Capture "mem" Word :> ReqBody '[JSON] Word8 :> Put '[JSON] Mem
  :<|> "inputs" :> QueryParam "starts" String :> Get '[JSON] [Input]
  :<|> "inputs" :> ReqBody '[JSON] String :> Post '[JSON] String
  :<|> "outputs" :> Get '[JSON] [Output]

